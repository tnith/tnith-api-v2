const express = require('express');
const path = require('path');
const logger = require('morgan');

const gameRouter = require('./routes/game');
const scoreRouter = require('./routes/score');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/score', scoreRouter);
app.use('/', gameRouter);

if (process.env.NODE_ENV === 'development') {
  app.use(logger('dev'));
  const testRouter = require('./routes/test');
  app.use('/test', testRouter);
}

module.exports = app;
