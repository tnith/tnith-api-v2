const siteFiles = [
  '/',
  '/index.html',
  '/resources/favicon.ico',
  '/resources/android-chrome-192x192.png',
  // not sure about caching app.js and sw.js, it seems it is really slower when removed
  '/publicapp.js',
  // '/sw.js',
  '/stylesheets/game.css',
  // maybe remove game.js (cf https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps/Offline_Service_workers)
  '/js/game.js',
  '/js/loadtitle.js',
  '/js/jquery/jquery-3.4.1.min.js',
  '/images/ban.svg',
  '/images/discord.png',
  '/images/gitlab.png',
  '/images/hay.png',
  '/images/needle.png',
  '/images/restart.png',
  '/images/spinner.gif',
  '/images/plancher/plancher600.jpg',
  '/images/plancher/plancher768.jpg',
  '/images/plancher/plancher992.jpg',
  '/images/plancher/plancher1200.jpg',
  '/images/plancher/plancher1600.jpg',
  '/images/plancher/plancher3840.jpg',
];

const cacheVersion = 'tnith-v2-cache-1.0.26';

const addResourcesToCache = async (resources) => {
    const cache = await caches.open(cacheVersion);
    await cache.addAll(resources);
  };
  
  const putInCache = async (request, response) => {
    const cache = await caches.open(cacheVersion);
    await cache.put(request, response);
  };
  
  const cacheFirst = async ({ request, preloadResponsePromise, fallbackUrl }) => {
    // First try to get the resource from the cache
    const responseFromCache = await caches.match(request);
    if (responseFromCache) {
      return responseFromCache;
    }
  
    // Next try to use the preloaded response, if it's there
    const preloadResponse = await preloadResponsePromise;
    if (preloadResponse) {
      console.info('using preload response', preloadResponse);
      putInCache(request, preloadResponse.clone());
      return preloadResponse;
    }
  
    // Next try to get the resource from the network
    try {
      const responseFromNetwork = await fetch(request);
      // response may be used only once
      // we need to save clone to put one copy in cache
      // and serve second one
      putInCache(request, responseFromNetwork.clone());
      return responseFromNetwork;
    } catch (error) {
      const fallbackResponse = await caches.match(fallbackUrl);
      if (fallbackResponse) {
        return fallbackResponse;
      }
      // when even the fallback response is not available,
      // there is nothing we can do, but we must always
      // return a Response object
      return new Response('Network error happened', {
        status: 408,
        headers: { 'Content-Type': 'text/plain' },
      });
    }
  };

  const networkFirst = async ({ request, type }) => {
    if (type == 'scoreRegistration') {
      // remplacer les fetch par des event.preloadResponse ? Pas sûr puisqu’on fait vraiment du
      // network first, donc rien qui peut être preloaded. Mais du coup on a une double requête ?
      return fetch(request).catch(function() {
        return new Response('Network error happened', { status: 408 });
      });
    } else {
      return fetch(request);
    }
  }
  
  const enableNavigationPreload = async () => {
    if (self.registration.navigationPreload) {
      // Enable navigation preloads!
      await self.registration.navigationPreload.enable();
    }
  };
  
  self.addEventListener('activate', (event) => {
    event.waitUntil(caches.keys().then((keyList) => {
      return Promise.all(keyList.map((key) => {
        if (key === cacheVersion) { return; }
        return caches.delete(key);
      }));
    }));
    event.waitUntil(enableNavigationPreload());
  });
  
  self.addEventListener('install', (event) => {
    event.waitUntil(
      addResourcesToCache(siteFiles)
    );
  });
  
  self.addEventListener('fetch', (event) => {
    const scoreRegex = new RegExp('^.*\/score$');
    if (event.request.method == 'POST' && scoreRegex.test(event.request.url)) {
      event.respondWith(
        networkFirst({
          request: event.request,
          type: 'scoreRegistration',
        })
      );
    } else {
      event.respondWith(
        cacheFirst({
          request: event.request,
          preloadResponsePromise: event.preloadResponse,
          fallbackUrl: '/public/images/plancher/plancher3840.jpg',
        })
      );
    }
  });