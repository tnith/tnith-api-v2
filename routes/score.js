const express = require('express');
const scoreRouter = express.Router();
const db = require('../db');

/* POST score. */
scoreRouter.post('/', function(req, res) {
  const pseudo = req.body.pseudo;
  const time  = req.body.time;

  // preparing the queries (not in order)
  const getScoreAfterInsertingQuery = {
    name: 'get-scores',
    text: `
      WITH ranking AS (SELECT dense_rank() OVER (ORDER BY time ASC) as rank, pseudo, time FROM scores ORDER BY time)
      (
        SELECT rank, pseudo, time
        FROM ranking
        WHERE time >= $1
        ORDER BY time ASC
        LIMIT 3
      )
      UNION
      (
        SELECT rank, pseudo, time
        FROM ranking
        WHERE time < $1
        ORDER BY time DESC
        LIMIT 2
      )
      ORDER BY rank
      `,
    values: [time]
  };
  
  const postScoreQuery = {
    name: 'post-score',
    text: 'INSERT INTO scores(pseudo, time) VALUES($1, $2)',
    values: [pseudo, time]
  };

  // executing the queries
  // We insert the score first: it may be slower, but the queries are way easier. Try to change it
  // for insterting when game is won, and just update when pseudo is submitted.
  db.query(postScoreQuery, (err, resp) => {
    if (err) {
      console.error('error: ', err.stack);
      res.status(500).send('score not registered D:');
    } else {
      db.query(getScoreAfterInsertingQuery, (err, resp) => {
        if (err) {
          console.error('error: ', err.stack);
        } else {
          res
          .status(200)
          .json(resp.rows);
          console.log('stored a score');
        }
      });
      console.log('result of request: ', resp.rows);
    }
  });

});

module.exports = scoreRouter;
